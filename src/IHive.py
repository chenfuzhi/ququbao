#coding:utf-8
'''
CREATE EXTERNAL TABLE hbase_table_1(key string, value string) STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler' WITH SERDEPROPERTIES ("hbase.columns.mapping" = "info:price") TBLPROPERTIES("hbase.table.name" = "item");
CREATE TABLE hbase_table_1(key int, value string)
STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
WITH SERDEPROPERTIES (
"hbase.columns.mapping" = "cf:string",
"hbase.table.name" = "hbase_table_0"
);
'''
import pyhs2

conn = pyhs2.connect(host='54.178.128.174', 
                     port=10000,
                     authMechanism="PLAIN", 
                     #user='root', 
                     #password='test', 
                     #database='default'
                    )
cur = conn.cursor()
cur.execute("show tables")
for i in cur.fetch():
    print i
cur.close()
conn.close()
    