#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
TEST_PATH = os.path.join(ROOT_PATH, "tests")

ENV = os.environ.get('ENV', 'DEV')
if ENV == '':
    ENV = 'DEV'

envs = {
    'DEV': {
        'MONGO_HOST': 'localhost:27017',
        'RECORD_URI': 'redis://localhost:6379/0',
        'LC_URI': 'redis://localhost:6379/11',
        'QUEUE_URI': 'redis://localhost:6379/11',
        'CACHE_URIS': [
            'redis://localhost:6379/5',
            'redis://localhost:6379/6',
            'redis://localhost:6379/7',
            'redis://localhost:6379/8',
        ],
        'AGGRE_URIS': [
            [
                'redis://localhost:6379/1',
                'redis://localhost:6379/2',
            ],
            [
                'redis://localhost:6379/3',
                'redis://localhost:6379/4',
            ],        
        ],
        'THRIFT_HOST': '54.178.128.174:9090',
    },
    'TEST': {
        'RECORD_URI': 'redis://ec2-54-199-147-18.ap-northeast-1.compute.amazonaws.com:6379/0',
        'QUEUE_URI': 'redis://ec2-54-199-147-18.ap-northeast-1.compute.amazonaws.com:6379/11',
        'AGGRE_URIS': [
        ],
        'DB_HOSTS': [
        ],
        'ES_HOSTS': ['ec2-54-199-164-67.ap-northeast-1.compute.amazonaws.com:9500'],
        'ADMIN_HOST': '54.199.145.250',
    },
}

for key, value in envs.get(ENV, envs['DEV']).iteritems():
    globals()[key] = value
