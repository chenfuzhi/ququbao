#coding:utf-8


from gevent import monkey; monkey.patch_all()
import gevent
from crawler.queues import ppi1, ppi2, ppi3, tbi1, tbi2, tbi3, pplist, tblist
from crawler.taobao.Clist import get_ids_deamon, list_cat
from crawler.paipai.Clist2 import get_sub_list
from crawler.paipai.CExtrItems import gen_list_url
from Ihbase2 import update_item        
import traceback



def main():
    import argparse
    parser = argparse.ArgumentParser(description='Call Worker with arguments')
    parser.add_argument('--model', '-m',  help='schd model (taobao, paipai, ...)', required=True)
    parser.add_argument('--type', '-t',  help='schd type (taobao, paipai, ...)', required=True)
    
    parser.add_argument('--poolsize', '-p', type=int, default=100, help='gevent pool size for worker (default: %(default)s)')
    option = parser.parse_args()
    
    if option.model == 'taobao':
        if option.type == 'list':
            pools = gevent.pool.Pool(int(option.poolsize))
            pools.spawn(list_cat, pools=pools)
            pools.join()
        elif option.type == 'ids':
            pools = gevent.pool.Pool(int(option.poolsize))
            pools.spawn(get_ids_deamon, pools=pools)
            pools.join()

    elif option.model == 'paipai':
        if option.type == 'list':
            pools = gevent.pool.Pool(int(option.poolsize))
            pools.spawn(get_sub_list, pools=pools)
            pools.join()
        elif option.type == 'ids':
            pz = int(option.poolsize)
            pools = gevent.pool.Pool(pz)
            for i in range(pz):
                pools.spawn(gen_list_url)
            pools.join()

if __name__ == "__main__":
    main()

