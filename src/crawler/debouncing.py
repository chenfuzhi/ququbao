#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Deboucing Algorithm

The Algorithm to make crawl or not decision smoother

Normally, we crawl if num_sold30 != 0, and skip 7 days if num_sold30 == 0
This strategy is far from ideal because the state switch is not very smooth.
Deboucing is about picking another key factor, smoothing our decisions made.

1. we save timestamp, lastday sales info into lastcheck
2. if lastday sales == 0, we have a clue that the item may not be sold later
   so we skip one day crawl -- save an 'offset' to lastcheck
3. we kept increase the offset if we kept getting 0s in lastday sales.
   unless we hit an offset of 15, or lastday sales goes nonzero

This algorithm is inspired by TCP Protocol's Slow Start & Fast Recovery Algorithms

Binary format:

63bit total
<--          31           --> <--     21     --> <- 4 -> <- 2 -> <-- 4 -->
         timestamp                last sales      offset   flag   not used

timestamp: a epoch timestamp
last_sales: last day sales figure
offset: whether we should delay the crawl
flag: 0 - version 0 format (58bit)
      1 - this version format
"""
import time

def fix_old_format(lastcheck):
    try:
        if len(bin(int(lastcheck))) == 58:
            return int(lastcheck) << 7
        else:
            return lastcheck
    except:
        return 

def new_lastcheck(lastcheck, info):
    """ given last binary and item info, generate new binary format
    """
    lastcheck = fix_old_format(lastcheck)
    last_sales_new = int(info.get('num_sold1') or '0')
    if not lastcheck:
        # this item has no lastcheck value, we will make a brand new for it
        # initialize offset according to solds
        # if the item isn't sold that good
        # we biased and offset it larger than 0 initially
        offset = 0
        if 'num_sold30' in info:
            ns = int(info.get('num_sold30', 0))
            if ns == 0:
                offset = 4
            elif ns < 30:
                offset = 1
        flag = 1
    else:
        # old item, adjust the values
        timestamp, last_sales, offset, flag = unpack(lastcheck)
        print info['id'], 'old offset', offset
        if flag == 0:
            flag = 1
        
        if last_sales_new != 0:
            # if last day has sales, we shouldn't delay the crawling
            offset = 0
        else: 
            # else we 'deboucing' it
            offset = 1 if offset == 0 else min(15, offset*2)

    print info['id'], 'new offset', offset
    return pack(int(time.time()), last_sales_new, offset, flag)
    
def can_update(lastcheck):
    if lastcheck is None:
        return True
    else:
        lastcheck = fix_old_format(lastcheck)
        timestamp, last_sales, offset, flag = unpack(lastcheck)
        ordinal = (timestamp+3600*8) // 86400
        ordinal_now = (int(time.time())+3600*8) // 86400
        if ordinal + offset + 1 <= ordinal_now:
            return True
    return False

def mask(count):
    return (1 << count) - 1

def allocate(value, max_bits, offset):
    """ allocate value at max max_bits """
    return (value & mask(max_bits)) << offset

def extract(value, max_bits, offset):
    """ extract value at max max_bits """
    return (value >> offset) & mask(max_bits)

def pack(timestamp, last_sales, offset, flag):
    """ pack info into 63bit integer lastcheck """
    return allocate(timestamp, 31, 32) | allocate(last_sales, 21, 11)\
            | allocate(offset, 4, 7) | allocate(flag, 2, 5)

def unpack(lastcheck):
    """ unpack 63bit lastcheck """
    try:
        lastcheck = int(lastcheck)
    except:
        return 0, 0, 0, 0

    return extract(lastcheck, 31, 32), extract(lastcheck, 21, 11),\
            extract(lastcheck, 4, 7), extract(lastcheck, 2, 5)


if __name__ == '__main__':
    import time
    now = int(time.time())
    print can_update(pack(now, 1, 0, 0))
    print can_update(pack(now-86400, 1, 0, 0))
