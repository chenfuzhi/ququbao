#coding:utf-8

import geventreactor; geventreactor.install()
from twisted.internet import reactor, defer
from twisted.web.http_headers import Headers
from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver

from twisted.web.client import getPage, HTTPClientFactory, Agent, nativeString

import gevent, time
from gevent import monkey; monkey.patch_socket()
import urllib2


cnt1, cnt2 = [0] *2
__url = "http://search.51job.com/job/60660756,c.html"

def greencount():
    global cnt1, cnt2
    while 1:
        try:
            ret = urllib2.urlopen(__url, timeout=30).read()
            cnt1 += 1
            print "                         greencount", len(ret), cnt1
        except Exception, e:
            print "gevent", e


@defer.inlineCallbacks
def txgetpage():
    global cnt1, cnt2
    while 1:
        try:
            ret = yield getPage(__url, timeout=30)
            cnt2 += 1
            print "############################txgetpage", len(ret), cnt2
        except Exception, e:
            print "twisted", e


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Call Worker with arguments')
    parser.add_argument('--site', '-s', choices=['taobao', 'paipai'], help='schd type (taobao, paipai, ...)', required=True)
    parser.add_argument('--type', '-t', choices=['xlist', 'item', 'shop'], help='schd worker type')
    parser.add_argument('--poolsize', '-p', type=int, default=100, help='gevent pool size for worker (default: %(default)s)')
    option = parser.parse_args()
    if option.site == "taobao":
        worker = ItemWorker(option.poolsize)
        worker.work()
    elif option.worker == "paipai":
        ShopWorker(option.poolsize).work()



if __name__ == "__main__":
    main()

    
    
    
    
    