#coding:utf-8

import re
import json
import time
import traceback
from datetime import datetime, timedelta

from crawler.queues import ppi1, ppi2, ppi3, tbi1, tbi2, tbi3, pplist, tblist
from crawler.taobao.h5 import get_item as get_item_h5, get_cid
from crawler.taobao.jsctx import get_data
import gevent
import gevent.pool


import requests



def get_item(itemid):
    i = get_item_h5(itemid)
    if 'error' not in i:
        i.update({'item_by_date':get_item_by_date_info(itemid, i)})
        if i['item_by_date']:
            try:
                lastday_info = i['item_by_date'][-2]
                i['num_sold1'] = lastday_info['num_sold1']
                i['wprice1'] = lastday_info['wprice1']
                i['wprice30'] = lastday_info['wprice30']
            except:
                i['num_sold1'], i['wprice1'], i['wprice30'] = 0, 0., 0.
    return i


def get_item_by_date_info(_id, info):
    ret = {}
    ret = requests.get('http://tds.alicdn.com/json/price_cut_data.htm?id={0}'.format(_id), timeout=15)
    new_cont = ret.content.replace('\n', '\t')
    if 'Server busy' in new_cont or 'Server Hangup' in new_cont or len(new_cont) == 0:
        print '!!!server busy'
        time.sleep(15)
        return get_item_by_date_info(_id, info)

    try:
        cont = re.compile('initChart\((.*)\);').match(new_cont).group(1)
    except:
        print new_cont, len(new_cont)
        raise
    js = get_data(cont)
    # item_by_date dict
    ibd = {}
    
    utc0 = datetime.utcnow()
    for i in range(1, 31):
        d = utc0 - timedelta(days=i)
        ibd.setdefault(d, {'sum_sale': 0, 'sum_num': 0, 'detail': {}})

    num30 = 0.
    sold30 = 0.
    for pricechart in js['price']:
        for slicedata in pricechart['data']:
            d = slicedata['x']
            d = d.replace(hour=8, minute=0, second=0, microsecond=0)
            ibd.setdefault(d, {'sum_sale': 0, 'sum_num': 0, 'detail': {}})
            ibd[d]['sum_sale'] += slicedata['y'] * slicedata['num']
            ibd[d]['sum_num'] += slicedata['num']
            ibd[d]['detail'][pricechart['name']] = slicedata['y']
            sold30 += slicedata['y'] * slicedata['num']
            num30 += slicedata['num']
            
    #print json.dumps(ibd, indent=4)
    ret = []
    for k, v in ibd.iteritems():
        r = {'wprice1': 0., 'wprice30': 0., 'num_sold1': 0, 'num_sold1_d': '',
             'date': k, 'num_sold30': info['num_sold30']}

        if v['sum_num'] != 0:
            r['wprice1'] = float(v['sum_sale']) / v['sum_num']
            r['num_sold1'] = v['sum_num']
            r['num_sold1_d'] = json.dumps(v['detail'])            

        if num30 != 0:
            r['wprice30'] = sold30 / num30
    
        ret.append(r)
    
    return sorted(ret, key=lambda x: x['date'])


def get_buyhistory(itemid):
    """ get buy history, first page only
    
    :param itemid: itemid for item
    :returns: list of (price/sold, sold/price, date) tuple
    """
    s = get_session()
    content = s.get('http://item.taobao.com/item.htm?id={}'.format(itemid)).content
    url = re.compile(r'detail:params="(.*?),showBuyerList').search(content).group(1)
    url += '&callback=jsonp'
    patjsonp = re.compile(r'jsonp\((.*?)\);?', re.DOTALL)
    s = get_blank_session()
    s.headers['Referer'] = 'http://detail.tmall.com/item.htm'
    try:
        content = s.get(url+'&callback=jsonp', timeout=30).content
        content = content.replace('\\"', '"')
        if content == 'jsonp({"status":1111,"wait":5})':
            print 'baned, sleeping for 5 mins'
            time.sleep(5*60)
            return get_buyhistory(itemid)
        ret1 = re.compile('<em class="tb-rmb-num">([^<]+)</em>.*?<td class="tb-amount">(\d+)</td>.*?<td class="tb-time">([^>]+)</td>', re.DOTALL).findall(content)
        ret2 = re.compile('<em>(\d+)</em>.*?<td>(\d+)</td>.*?<td>([^<]+)</td>', re.DOTALL).findall(content)
        ret1.extend(ret2)
        return ret1
    except:
        print '!!!', itemid
        traceback.print_exc()

def get_lastbuy(itemid):
    history = get_buyhistory(itemid)
    return max([h[-1] for h in history])

def get_offset(itemid):
    now = datetime.now()
    try:
        lb = datetime.strptime(get_lastbuy(itemid), '%Y-%m-%d %H:%M:%S')
    except:
        traceback.print_exc()
        return None
    return (now - lb).days

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Get item by id')
    parser.add_argument('--id', '-i', type=int, help='taobao item id, e.g. 21825059650', required=True)
    option = parser.parse_args()
    from pprint import pprint
    pprint(get_item(option.id))

if __name__ == '__main__':
    main()
