#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gevent import monkey; monkey.patch_all()
import gevent
import gevent.pool

import re
import time
import random
import traceback
from itertools import chain
from functools import partial
from crawler.queues import ppi1, ppi2, ppi3, tbi1, tbi2, tbi3, pplist, tblist
import json
#from session import get_session, get_blank_session
import requests

def get_count(data):
    """ get totalNumber from taobao's json return """
    if data:
        total = data['selectedCondition']['totalNumber']
        return 9500 if u'万' in total else int(total)
    else:
        return 0

def get_ids(data):
    if data and data['itemList']:
        return [ int(i['itemId']) for i in data['itemList'] ]
    else:
        return []

def get_subcats(data):
    if data['cat']:
        if data['cat']['hasGroup'] == 0:
            return [int(d['value']) for d in data['cat']['catList']]
        else:
            cids = []
            for l in data['cat']['catGroupList']:
                for d in l['catList']:
                    cids.append(int(d['value']))
            return cids
    else:
        return []

def get_json(cid, paths=[], page=1, sort=None):
    """ given cid and (optional) paths/page/sort, return json """
    url = 'http://list.taobao.com/itemlist/default.htm?'
    params = {
        'pSize': 95,
        'json': 'on',
    }
    if cid:
        params['cat'] = cid
    if paths:
        paths = [p for p in paths if p]
        params['ppath'] = ';'.join(paths)
    if sort:
        params['sort'] = sort
    if page > 1:
        params['s'] = (page-1)*95
    try:
        data = requests.get(url, params=params, timeout=30).json()
    except:
        data = None
    return data


def get_ids_deamon(pools=gevent.pool.Pool(100)):
    def on_ids():
        while 1:
            dt = json.loads(tblist.get())
            data = get_json(dt.pop('cid'), **dt)
            ids = get_ids(data)
            tbi2.put(*ids)
            print 'put {} to tbi2'.format(len(ids))
    while pools.free_count():
        print 'start on_ids..'
        pools.spawn(on_ids)

sum_cnt = 0
def list_cat(cid=50035966, paths=[], pools=gevent.pool.Pool(100)):
    
    def gen_pages(data):
        d = {'cid':cid, 'paths':paths}
        total = data['selectedCondition']['totalNumber']
        if u'万' in total:
            pages = 100
        else: 
            total = int(total)
            pages = total / 100 + 1 if (total % 100) else 0
        ps = []
        #[(d['page'] = i; ps.append(json.dumps(d)) for i in range(2, pages + 1)]
        for i in range(2, pages + 1):
            d['page'] = i
            ps.append(json.dumps(d))
        print tblist.put(*ps)
        print tbi2.put(*get_ids(data))
        
    global sum_cnt
    data = get_json(cid, paths=paths)
    if data:
        total = data['selectedCondition']['totalNumber']
        if u'万' in total or int(total)>9500:
            if data['cat']:
                cids = []
                if data['cat']['hasGroup'] == 0:
                    cids = [int(d['value']) for d in data['cat']['catList']]
                else:
                    for l in data['cat']['catGroupList']:
                        for d in l['catList']:
                            cids.append(int(d['value']))
                [pools.spawn(list_cat, cid=c, paths=paths, pools=pools) for c in cids]
            else:
                if data['propertyList']:
                    allpath = [ [p2['value'] for p2 in p1['propertyList']] for p1 in data['propertyList'] ]
                    allpath = sorted(allpath, key=len, reverse=True)
                    if len(paths) == 0:
                        for pp in allpath:
                            for p in pp:
                                pools.spawn(list_cat, cid=cid, paths=[p], pools=pools)
                    else:
                        for p in paths:
                            [allpath.remove(pp) for pp in allpath if p in pp]
                        if allpath:
                            for p1 in allpath[0]:
                                print [p1] + paths
                                list_cat(cid=cid, paths=paths + [p1], pools=pools)
                        else:
                            gen_pages(data)
                else:
                    gen_pages(data)
        else:
            gen_pages(data)
    else:
        print 'data is none'


if __name__ == '__main__':
    pools = gevent.pool.Pool(300)
    pools.spawn(list_cat, pools=pools)
    pools.join()

    