#!/usr/bin/env 
# -*- coding: utf-8 -*-
from gevent import monkey; monkey.patch_all()
from gevent.pool import Pool, Group

from crawler.tbcat import get_json
from crawler.cates import cates, need_crawl

import re
import traceback

def find_cids(cid=0, catdict={}, pool=None, group=None, on_cid=None):
    # print('geting cids from cid {}'.format(cid))
    data = get_json(cid=cid)
    if not data:
        return

    if cid not in catdict:
        catdict[cid] = {}
    
    try:
        total = catdict[cid]['total'] = data['selectedCondition']['totalNumber']
    except:
        total = ''
        
    try:
        scid = catdict[cid]['scid'] = int(re.compile('scatid=(\d+)').search(data['p4p']['oldApi']).group(1))
    except:
        scid = catdict[cid]['scid'] = 0

    if u'万' in total:
        ntotal = int(float(total[:-1])*10000)
    else:
        ntotal = int(float(total))

    if ntotal>5000 and need_crawl(scid):
        name = catdict[cid].get('name', 'Unknown')
        print(u'found {}, {}({})'.format(cid, name, total))
        if on_cid is not None:
            try:
                if pool is not None:
                    pool.spawn(on_cid, cid)
                else:
                    on_cid(cid)
            except:
                traceback.print_exc()
            
    def add_cat(c):
        cid, name, children = int(c['value']), c['name'], int(c['hasChild'])
        if u'万' in total:
            catdict[cid] = {'name':name, 'children':children}

        if cid in catdict and 'scid' not in catdict[cid]:
            if group is not None:
                group.spawn(find_cids, cid, catdict, pool, group, on_cid)
            else:
                find_cids(cid, catdict, pool, group, on_cid)

    if 'cat' in data:
        cat = data['cat']
        if cat:
            if cat['hasGroup'] == 0:
                for c in cat['catList']:
                    add_cat(c)
            else:
                for l in cat['catGroupList']:
                    for c in l['catList']:
                        add_cat(c)


if __name__ == '__main__':
    pool = Pool(20)    
    group = Group()
    find_cids(0, pool=pool, group=group)
    pool.join()
    group.join()
