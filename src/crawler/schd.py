#coding:utf-8

from gevent import monkey; monkey.patch_all()
import gevent
from crawler.queues import ppi1, ppi2, ppi3, tbi1, tbi2, tbi3, pplist, tblist
from crawler.taobao.Clist import list_cat
from crawler.taobao.Citem import get_item
from IMongo import update_item        
import traceback
import time


def tb_list(pools=None):
    pools.spawn(list_cat, pools=pools)
    pools.join()


def tb_items(pools=None):
    def on_item():
        while 1:
            queues = sorted([tbi1, tbi2, tbi3], key=lambda x:x.priority, reverse=True)
            for q in queues:
                while 1:
                    itemid = q.get(block=False)
                    if itemid is not None:
                        try:
                            data = get_item(itemid)
                            ret = update_item(data, tp='tb')
                            print ret, "ok", pools.free_count() 
                        except:
                            traceback.print_exc()
                    else:
                        break
            print 'wait 3 second'
            time.sleep(3)

    while pools.free_count():
        print 'start get_item..'
        pools.spawn(on_item)

def pp_items():
    pass

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Call Worker with arguments')
    parser.add_argument('--model', '-m',  help='schd type (taobao, paipai, ...)', required=True)
    parser.add_argument('--call', '-c', help='schd worker type')
    parser.add_argument('--args', '-a', nargs='+', type=str, help='call args... eg: 11, 22, 33', default=[])
    parser.add_argument('--poolsize', '-p', type=int, default=100, help='gevent pool size for worker (default: %(default)s)')
    option = parser.parse_args()

    if option.model == 'taobao':
        pools = gevent.pool.Pool(int(option.poolsize))
        pools.spawn(tb_items, pools=pools)
        pools.join()
    elif option.model == 'paipai':
        pools = gevent.pool.Pool(int(option.poolsize))
        pools.spawn(tb_items, pools=pools)
        pools.join()

if __name__ == "__main__":
    main()

