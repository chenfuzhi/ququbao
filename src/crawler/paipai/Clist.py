#coding:utf-8

from gevent import monkey; monkey.patch_all()
import requests
import gevent, time

import gevent.pool
import urllib2
from pyquery import PyQuery as pq
from lxml import etree
import traceback
import redis
server = redis.Redis()

'''
http://s.paipai.com/0,226060/s---1-60-77-226060--1-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html
http://s.paipai.com/0,226060/s---61-60-77-226060--1-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html
http://s.paipai.com/0,226060/s---121-60-77-226060--1-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html
http://s.paipai.com/0,226060/s---181-60-77-226060--1-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html
'''

cnt_sum = 0
pools = gevent.pool.Pool(300)

def get_list():
    pagesize = 60
    pagemax = 100
    
    url = 'http://s.paipai.com/0,226060/s---1-60-77-226060--3-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html'
    ret = requests.get(url)
    p = pq(ret.content.decode('gbk'))
    cnt = p.find('div#ppCrumb .result strong').text()
    print cnt
    
    global pools
    print "#########################", pools.free_count()
    lis = p.find('div#classNavArea .attr_values ul li span')
    #for li in lis:
        #get_sub_list('0,{}'.format(pq(li).attr('classid')), pq(li).attr('classid'))
        #break
    #http://s.paipai.com/0,226060-0,226066-55,6904/s---1-60-77-226066--3-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html
    #http://s.paipai.com/0,226060-0,226066-55,6904-40708,1/s---1-60-77-226066--3-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html
    #get_sub_list('0,226066-55,6904-40708,1', '226066')
    get_sub_list(mcat='226060', attrs='0,226064', scat='226064')
    
def get_sub_list(mcat='226060', attrs='', scat=''):
    try:
        global pools, server
        burl = 'http://s.paipai.com/0,{}-{}/s---{}-60-77-{}--3-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html'
        if attrs == '':
            burl = 'http://s.paipai.com/0,{}/s---{}-60-77-{}--3-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html'
            url = burl.format(mcat, 1, mcat)
        else:
            url = burl.format(mcat, attrs, 1, scat)

        ret = requests.get(url)
        try:
            p = pq(ret.content.decode('gbk'))
        except:
            p = pq(ret.content)
        cnt = p.find('div#ppCrumb .result strong').text()
        
        print "############", cnt, "===", url
        if int(cnt) > 6000:
            lis = p.find('div#classNavArea .attr_values ul li span')
            if len(lis) > 0:
                print "============"
                for li in lis:
                    print "=====~"
                    #get_sub_list(mcat, '0,{}'.format(pq(li).attr('classid')), pq(li).attr('classid'))
                    pools.spawn(get_sub_list, mcat, '0,{}'.format(pq(li).attr('classid')), pq(li).attr('classid'))
            else:
                _attrs = propertyList = p.find('div#propertyList .attr')
                if len(_attrs) > 0:
                    print "^^^^^", url
                    for attr in _attrs:
                        for span in pq(attr).find('div.attr_values ul li span'):
                            print "|||||"
                            paths = [attrs, '{},{}'.format(*[pq(span).attr(at) for at in ['attrid', 'attritemid']])]
                            pools.spawn(get_sub_list, mcat, '-'.join(paths), scat)
                else:
                    brandSelList = p.find('div#brandSelList .attr_values ul li span')
                    if len(brandSelList) > 0:
                        for span in brandSelList:
                            paths = [attrs, '{},{}'.format(*[pq(span).attr(at) for at in ['attrid', 'attritemid']])]
                            pools.spawn(get_sub_list, mcat, '-'.join(paths), scat)
        else:
            pagesize = 60
            cnt = int(cnt)
            pages = (cnt / pagesize) + ( 1 if cnt % pagesize else 0) 
            for i in range(0, pages):
                #print url.format(catids, pagesize*i+1, searchid), cnt
                server.sadd('list_urls', burl.format(mcat, attrs, pagesize*i+1, scat))
                #pools.spawn(gen_list_url, url.format(catids, pagesize*i+1, searchid))
    except:
        traceback.print_exc()
        #print 'catids  {} '.format(catids), pools.free_count(), url.format(catids, 1, searchid)
        get_sub_list(mcat, attrs, scat)
        time.sleep(1)

def gen_list_url():
    
    while 1:
        try:
            global server
            url = server.spop('list_urls')
            if url is None:
                time.sleep(3)
                print 'sleep, 3 sceond'
                continue
            ret = requests.get(url)
            p = pq(ret.content.decode('gbk'))
            print 'gen_list_url', len(p.find('div#commList .hproduct'))
            for x in [pq(pd).attr('commid') for pd in p.find('div#commList .hproduct')]:
                server.sadd('items', x)
                server.lpush('litem', x)
        except:
            traceback.print_exc()
            


if __name__ == "__main__":
    pools.spawn(get_list)
    pools.join()
    
    
    
    
    
    
    
    