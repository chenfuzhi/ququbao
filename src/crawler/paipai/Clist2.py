#coding:utf-8
from gevent import monkey; monkey.patch_all()
import requests
import gevent, time

import gevent.pool
import urllib2
from pyquery import PyQuery as pq
from lxml import etree
import traceback
import redis
from crawler.queues import ppi1, ppi2, ppi3, tbi1, tbi2, tbi3, pplist, tblist

server = redis.Redis()

'''
http://s.paipai.com/0,226060/s---1-60-77-226060--1-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html
http://s.paipai.com/0,226060/s---61-60-77-226060--1-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html
http://s.paipai.com/0,226060/s---121-60-77-226060--1-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html
http://s.paipai.com/0,226060/s---181-60-77-226060--1-4-3----2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html
'''

cnt_sum = 0
#pools = gevent.pool.Pool(300)

    
def get_sub_list(mcat='226060', prices=[0, 0], pstep=0, scat='', pools=gevent.pool.Pool(100)):
    try:
        global server, cnt_sum
        burl = 'http://s.paipai.com/0,{}/s---{}-60-24-{}--3-4-3--{}-{}-2-2-512-128-1-0-as,1-bpf,0-cd,1-platform,1-sf,21.html'
        if scat == '':
            url = burl.format(mcat, 1, mcat, *prices)
        else:
            url = burl.format('{}-0,{}'.format(mcat, scat), 1, scat, *prices)
        ret = requests.get(url)
        try:
            p = pq(ret.content.decode('gbk'))
        except:
            p = pq(ret.content)
        err = p.find('div.mod_msg')
        if err: return
        
        cnt = p.find('div#ppCrumb .result strong').text()
        
        print "############", cnt, cnt_sum, "===", url
        if int(cnt) > 6000:
            if prices == [0, 0]:
                step = 100
                for i in range(0, 100000, step):
                    pools.spawn(get_sub_list, mcat, [i, i + step - 1], step, scat, pools)
                pools.spawn(get_sub_list, mcat, [100000, 0], step, scat, pools)
            else:
                if prices[1] == 0 and prices[0] != 0:
                    print "###########################################"
                else:
                    print prices, pstep
                    if pstep == 100:  # 0, 99 => (0, 9)(10, 19)(20, 29)
                        for i in range(0, 9):
                            pools.spawn(get_sub_list, mcat, [prices[0]+i*10, prices[0]+i*10+9], 10, scat, pools)
                    elif pstep == 10: # 0, 9 => (0,1),(1,2),(2,3),(3,4),(4,5),(5,6),(6,7),(7,8),(8,9)
                        for i in range(0, 9):
                            pools.spawn(get_sub_list, mcat, [prices[0]+i, prices[0]+i+1], 1, scat, pools)
                    else:
                        lis = p.find('div#classNavArea .attr_values ul li span')
                        print "=====~", cnt, url
                        if len(lis) > 0:
                            for li in lis:
                                print mcat, prices, pstep, pq(li).attr('classid')
                                pools.spawn(get_sub_list, mcat, prices=prices, pstep=pstep, scat=pq(li).attr('classid'), pools=pools)
                        else:
                            print "###########################################"
        else:
            pagesize = 60
            cnt = int(cnt)
            cnt_sum += cnt
            pages = (cnt / pagesize) + ( 1 if cnt % pagesize else 0)
            pplist.put(*[burl.format(mcat, pagesize*i+1, mcat, *prices) for i in range(0, pages)])
    except:
        traceback.print_exc()
        time.sleep(1)



if __name__ == "__main__":
    pools.spawn(get_sub_list)
    pools.join()
    
    
    
    
    
    
    
    