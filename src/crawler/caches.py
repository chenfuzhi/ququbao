#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import time
import redis
import binascii
import traceback

from settings import LC_URI
import redis
import debouncing

host, port, db = re.compile('redis://(.*):(\d+)/(\d+)').search(LC_URI).groups()
conn = redis.Redis(host=host, port=int(port), db=int(db))



class LC(object):
    hashkey = 'lastcheck-{}'
    
    @staticmethod
    def count(type):
        return LC.gethash(type).count()

    @staticmethod
    def delete(type, id):
        return LC.gethash(type).hdel(id)

    @staticmethod
    def need_update(type, *ids):
        if not ids:
            return []
        ids = list(set(ids))

        if type == 'item':
            ids = [ ids[i] for i, wc in enumerate(WC.contains(*ids)) if not wc ]
            if not ids:
                return []

        thehash = LC.gethash(type)
        tsnow = time.time()
        if len(ids) == 1:
            lastchecks = [thehash.hget(ids[0])]
        else:
            lastchecks = thehash.hmget(*ids)

        needs = []
        for i, lastcheck in enumerate(lastchecks): 
            if type == 'item':
                if lastcheck is None or debouncing.can_update(lastcheck):
                    needs.append(ids[i])
            else:
                if lastcheck is None or float(lastcheck) + 86400*7 < tsnow:
                    needs.append(ids[i])
        return needs

    @staticmethod
    def update_if_needed(type, id, on_update, queue):
        """ try update item by id, update lastcheck if needed """
        hashkey = LC.hashkey.format(type)
        tsnow = time.time()
        
        today_key = time.strftime("%Y-%m-%d", time.gmtime())
        
        if LC.need_update(type, int(id)):
            info = None
            conn_record.hincrby(today_key, '{}:crawl-sum'.format(type))
            try:
                info = on_update(id)
            except:
                conn_record.hincrby(today_key, '{}:crawl-err-except'.format(type))
                print('we do not set task_done for id {}, so we will pick them up in requeue'.format(id))
                traceback.print_exc()
            else:
                conn_record.hincrby(today_key, '{}:crawl-return'.format(type))
                queue.task_done(id)
                if type == "item":
                    if not info or not info.has_key('num_sold30'):
                        conn_record.hincrby(today_key, '{}:crawl-data-err'.format(type))
                        print "item err:", info
                        return
                    conn_record.hincrby(today_key, '{}:crawl-success'.format(type))

                    # now we update deboucing info
                    lastcheck = LC.gethash(type).hget(id)
                    new_lastcheck = debouncing.new_lastcheck(lastcheck, info)
                    LC.gethash(type).hset(id, new_lastcheck)
                else:
                    LC.gethash(type).hset(id, tsnow)
        else:
            queue.task_done(id)


