#coding:utf-8
'''
CREATE EXTERNAL TABLE hbase_table_1(key string, value string) STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler' WITH SERDEPROPERTIES ("hbase.columns.mapping" = "info:price") TBLPROPERTIES("hbase.table.name" = "item");
CREATE TABLE hbase_table_1(key int, value string)
STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
WITH SERDEPROPERTIES (
"hbase.columns.mapping" = "cf:string",
"hbase.table.name" = "hbase_table_0"
);
'''
import pymongo
from settings import MONGO_HOST
host, port = MONGO_HOST.split(':')
conn = pymongo.Connection(host=host, port=int(port))

def update_item(item, tp=''):
    #conn.item.save(item)
    ret = conn.ququbao.item.update({'id':item['id']}, item, upsert=True)
    return ret
