#coding:utf-8



import happybase
from happybase import ConnectionPool
from starbase import Connection
import pprint
import time
import traceback
from datetime import datetime

from settings import THRIFT_HOST

#host, port = THRIFT_HOST.split(':')
#hbase = happybase.ConnectionPool(size=100, host=host, port=int(port))


def init_tables(conn):
    table_name = 'item'
    families = {
        'info':dict(max_versions=1, block_cache_enabled=False),
        'date':dict(max_versions=1, block_cache_enabled=False),
    }
    try:
        conn.create_table(table_name, families)
    except happybase.hbase.ttypes.AlreadyExists, e:
        #conn.disable_table(table_name)
        #conn.delete_table(table_name)
        print e
        
def update_item(item, tp=''):
    host, port = THRIFT_HOST.split(':')
    conn = Connection(host=host, port=8080)
    table = conn.table('item')
    row_key = '{}-{}'.format(tp, item['id'])
    
    print item['title'].encode('utf-8')
    
    t = time.strftime('%Y-%m-%d')
    for k, v in item.iteritems():
        if type(v) is unicode:
            item[k] = v.encode('utf-8')
        if type(v) is int:
            item[k] = str(v)
    d = {
        'info:title': item['title'],
        'info:price': item['price'],
        'info:type': item['type'],
        'info:cid': str(item['cid']),
        'info:brand': item['brand'],
        'info:image': item['image'],
        'info:shopid': item['shopid'],
        'info:status': 'a',
        }
    
    for s in item['item_by_date']:
        for k in ['num_sold1', 'wprice1']:
            ck = 'date:{}:{}'.format(s['date'].strftime('%Y-%m-%d'), k)
            d.update({ck:str(s[k])})
            
    #pprint.pprint(d)
    #print row_key
    try:
        return table.insert(row_key, d)
    except:
        traceback.print_exc()
    

def main():
    #from crawler.taobao.Citem import get_item
    #data = get_item(38487459169)
    #update_item(data, type='tb')    
    #init_tables(hbase)
    host, port = THRIFT_HOST.split(':')
    #conn = happybase.Connection(host, port=int(port))
    conn = Connection(host=host, port=8080)
    table = conn.table('aaa')
    table.insert('aaaaa', {'f1:aa':'aaaaddd'})
    print conn.tables()

if __name__ == "__main__":
    main()





